class Students {
    constructor(nama) {
        this._nama = nama;
    }

    get nama(){
        return this._nama;
    }

    set nama(nama) {
        this._nama = nama;
    }
}

export default Students;