class Kelas {
    constructor(kelas, level, pengajar) {
        this._kelas = kelas;
        this._level = level;
        this._pengajar = pengajar;
        this._murid = [];
    }

    get kelas() {
        return this._kelas;
    }

    set kelas(kelas){
        this._kelas = kelas;
    }
    
    get level() {
        return this._level;
    }

    set level(level){
        this._level = level;
    }

    get pengajar() {
        return this._pengajar;
    }

    set pengajar(pengajar){
        this._pengajar = pengajar;
    }

    get murid() {
        return this._murid;
    }

    addMurid(lol) {
        this._murid.push(lol)
    }
}

export default Kelas;