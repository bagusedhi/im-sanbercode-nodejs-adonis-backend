import Kelas from './kelas';

class Bootcamp {
    constructor(nama) {
        this._nama = nama;
        this._kelas = [];
    }

    get nama(){
        return this._nama;
    }

    set nama(nama){
        this._nama = nama;
    }

    get kelas() {
        return this._kelas;
    }

    createClass (kelas, level, pengajar){
        let kelas1 = new Kelas(kelas, level, pengajar);
        this._kelas.push(kelas1);
    }

    register (kelas, newStud) {
        let cariKelas = this._kelas.find((kel) => kel.kelas === kelas);
        cariKelas.addMurid(newStud); 
    }
}

export default Bootcamp;