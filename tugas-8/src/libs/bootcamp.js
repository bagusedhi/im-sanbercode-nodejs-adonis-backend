import Employee from "./employee";
import fs from 'fs';
const path = "data.json";
import fsPromise from "fs/promises"

class Bootcamp {
    static register(input) {
        let [name, password, role] = input.split(",")
        fs.readFile(path,(error, data) => {
            if(error) {
                console.log(error);
            }

            let existData = JSON.parse(data);
            let employee = new Employee(name, password, role);
            existData.push(employee);
            fs.writeFile(path, JSON.stringify(existData), (err) => {
                if(err) {
                    console.log(err);
                } else {
                    console.log("Berhasil Register");
                }
            })
        })
    }

    static login(input) {
        let [name, password] = input.split(",");

        fsPromise.readFile(path)
        .then((data) => {
            let employees = JSON.parse(data);

            let indexEmp = employees.findIndex((emp) => emp._name == name);
            if (indexEmp == -1) {
                console.log("Data tidak ditemukan");
            } else {
                let employee = employees[indexEmp];

                if (employee._password == password) {
                    employee._isLogin = true;

                    employees.splice(indexEmp, 1, employee);

                    return fsPromise.writeFile(path, JSON.stringify(employees));
                } else {
                    console.log("Password salah");
                }
            }
        })
        // .then(() => {
        //     console.log("Berhasil login");
        // })
        .catch((err) => {
            console.log(err);
        })
    }
}

export default Bootcamp;