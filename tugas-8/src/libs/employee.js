class Employee {
    constructor(name, password, role){
        this._name = name;
        this._password = password;
        this._role = role;
        this._isLogin = false;
    }
    get name() {
        return this._name;
    }

    set name(name) {
        this._name = name;
    }

    get password() {
        return this._password;
    }

    set password(password) {
        this._password = password;
    }

    get role() {
        return this._role;
    }

    set role(role) {
        this._role = role;
    }

    get isLogin() {
        return this._isLogin;
    }

    set isLogin(isLogin) {
        this._isLogin = isLogin;
    }
}

export default Employee;