export const sapa = (nama) => {
    return `halo selamat pagi, ${nama}`;
}

export const convert = (nama, domisili, umur) => {
    return {
        nama,
        domisili,
        umur
    }
}

export const checkScore = (score) => {
    let spliter = score.split(",").reduce((obj, str, index) => {
        let strParts = str.split(":");
        if (strParts[0] && strParts[1]) {
            obj[strParts[0].replace(/\s+/g, '')] = strParts[1].trim();
        }
        return obj;
    }, {});

    return spliter;
}


const data1 = [{
        name: "Ahmad",
        kelas: "adonis"
    },
    {
        name: "Regi",
        kelas: "laravel"
    },
    {
        name: "Bondra",
        kelas: "adonis"
    },
    {
        name: "Iqbal",
        kelas: "vuejs"
    },
    {
        name: "Putri",
        kelas: "Laravel"
    }
]

export const filtData = (kelas) => {
    return data1.filter((el) => el['kelas'].toLowerCase().includes(kelas.toLowerCase()));
}