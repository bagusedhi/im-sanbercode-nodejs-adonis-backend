import {
    sapa,
    convert,
    checkScore,
    filtData
} from './libs/soal';

const myArgs = process.argv.slice(2);
const command = myArgs[0];
console.log('myArgs: ', myArgs);

switch (command) {
    case 'sapa':
        let nama1 = myArgs[1];
        console.log(sapa(nama1));
        break;
    case 'convert':
        const params = myArgs.slice(1);
        let [nama, domisili, umur] = params;
        console.log(convert(nama, domisili, umur));
        break;
    case 'checkScore':
        let score = myArgs[1];
        console.log(checkScore(score));
        break;
    case 'filtData':
        let kelas = myArgs[1];
        console.log(filtData(kelas));
        break;
    default:
        break;
}