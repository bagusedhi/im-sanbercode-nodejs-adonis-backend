/* --------------------------------- soal 1 --------------------------------- */
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';
console.log(word+" "+second+" "+third+" "+fourth+" "+fifth+" "+sixth+" "+seventh);
console.log('\n');

/* --------------------------------- soal 2 --------------------------------- */
var sentence = "I am going to be React Native Developer"; 

var exampleFirstWord = sentence[0] ; 
var exampleSecondWord = sentence[2] + sentence[3]  ; 
var thirdWord = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9];
var fourthWord = sentence[11] + sentence[12]; 
var fifthWord = sentence[14] + sentence[15];
var sixthWord = sentence[sentence.indexOf("React")] + sentence[sentence.indexOf("React") + 1] + sentence[sentence.indexOf("React") + 2] + sentence[sentence.indexOf("React") + 3] + sentence[sentence.indexOf("React") + 4];
var seventhWord = sentence[sentence.indexOf("Native")] + sentence[sentence.indexOf("Native") + 1] + sentence[sentence.indexOf("Native") + 2] + sentence[sentence.indexOf("Native") + 3] + sentence[sentence.indexOf("Native") + 4] + sentence[sentence.indexOf("Native") + 5];
var eighthWord = sentence[sentence.indexOf("Developer")] + sentence[sentence.indexOf("Developer") + 1] + sentence[sentence.indexOf("Developer") + 2] + sentence[sentence.indexOf("Developer") + 3] + sentence[sentence.indexOf("Developer") + 4] + sentence[sentence.indexOf("Developer") + 5] + sentence[sentence.indexOf("Developer") + 6] + sentence[sentence.indexOf("Developer") + 7] + sentence[sentence.indexOf("Developer") + 8];
// var thirdWord = sentence.substr(5,5);
// var fourthWord = sentence.substr(11,2); 
// var fifthWord = sentence.substr(14,2);
// var sixthWord = sentence.substr(sentence.indexOf("React"),5);
// var seventhWord = sentence.substr(sentence.indexOf("Native"),6);
// var eighthWord = sentence.substr(sentence.indexOf("Developer"),9);

console.log('First Word: ' + exampleFirstWord); 
console.log('Second Word: ' + exampleSecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fifthWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 
console.log('Eighth Word: ' + eighthWord)
console.log('\n');
/* --------------------------------- soal 3 --------------------------------- */
var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(sentence2.indexOf("JavaScript"), sentence2.indexOf("JavaScript") + 10);
var thirdWord2 = sentence2.substring(sentence2.indexOf("is"), sentence2.indexOf("is") + 2); // do your own! 
var fourthWord2 = sentence2.substring(sentence2.indexOf("so"), sentence2.indexOf("so") + 2); 
var fifthWord2 = sentence2.substring(sentence2.indexOf("cool"), sentence2.indexOf("cool") + 4);

console.log('First Word: ' + exampleFirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
console.log('\n');

/* --------------------------------- soal 4 --------------------------------- */
var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(sentence3.indexOf("JavaScript"), sentence3.indexOf("JavaScript") + 10); // do your own! 
var thirdWord3 = sentence3.substring(sentence3.indexOf("is"), sentence3.indexOf("is") + 2); // do your own! 
var fourthWord3 = sentence3.substring(sentence3.indexOf("so"), sentence3.indexOf("so") + 2); // do your own! 
var fifthWord3 = sentence3.substring(sentence3.indexOf("cool"), sentence3.indexOf("cool") + 4); // do your own! 

var firstWordLength = exampleFirstWord3.length;
// lanjutkan buat variable lagi di bawah ini 
var secondWordLength = secondWord3.length;
var thirdWordLength = thirdWord3.length;
var fourthWordLength = fourthWord3.length;
var fifthWordLength = fifthWord3.length;

console.log('First Word: ' + exampleFirstWord3 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3 + ', with length: ' + fifthWordLength); 