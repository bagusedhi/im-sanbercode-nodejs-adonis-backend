/* --------------------------------- if else -------------------------------- */
var nama = "John";
var peran = "Guard";

if (nama != "") {
    if (peran != "") {
        if (peran.toLowerCase() == "penyihir") {
            console.log("Selamat datang di Dunia Werewolf, " + nama);
            console.log("Halo "+peran+" "+nama+", kamu dapat melihat siapa yang menjadi werewolf!");
        } else if (peran.toLowerCase() == "guard") {
            console.log("Selamat datang di Dunia Werewolf, " + nama);
            console.log("Halo "+peran+" "+nama+", kamu akan membantu melindungi temanmu dari serangan werewolf.");
        } else if (peran.toLowerCase() == "werewolf") {
            console.log("Selamat datang di Dunia Werewolf, " + nama);
            console.log("Halo "+peran+" "+nama+", Kamu akan memakan mangsa setiap malam!");
        } else {
            console.log("Selamat datang di Dunia Werewolf, " + nama);
            console.log("Peran "+peran+" belum tersedia, silakan pilih peran yang tersedia!");
        }
    } else {
        console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!");
    }
} else {
    console.log("Nama harus diisi!");
}
console.log('\n');

/* ------------------------------- switch case ------------------------------ */
var tanggal = 20;
var bulan = 11;
var tahun = 2012;
var newTanggal;
var namaBulan;

if (tanggal < 1 || tanggal > 31) {
    newTanggal = "1";
} else {
    newTanggal = tanggal;
}

switch (bulan) {
    case 1: namaBulan = "Januari";
    break;
    case 2: namaBulan = "Februari";
    break;
    case 3: namaBulan = "Maret";
    break;
    case 4: namaBulan = "April";
    break;
    case 5: namaBulan = "Mei";
    break;
    case 6: namaBulan = "Juni";
    break;
    case 7: namaBulan = "Juli";
    break;
    case 8: namaBulan = "Agustus";
    break;
    case 9: namaBulan = "September";
    break;
    case 10: namaBulan = "Oktober";
    break;
    case 11: namaBulan = "November";
    break;
    case 12: namaBulan = "Desember";
    break;
    default: namaBulan = "Januari";
}

console.log(newTanggal+" "+namaBulan+" "+tahun);