/* ---------------------------------- no 1 ---------------------------------- */
console.log("LOOPING PERTAMA");
var i = 2;
while (i <= 20) {
    console.log(i + " - I love coding");
    i += 2;
}

console.log("LOOPING KEDUA");
while (i > 0) {
    console.log(i + " - I love coding");
    i -= 2;
}
console.log("--------------------------------------------------------------------");

/* ---------------------------------- no 2 ---------------------------------- */
for (var index = 1; index <= 20; index++) {
    if (index % 2 == 0) {
        console.log(index + " - Berkualitas");
    } else if (index % 3 == 0) {
        console.log(index + " - I Love Coding");
    } else if (index % 2 == 1) {
        console.log(index + " - Santai");
    }
}
console.log("--------------------------------------------------------------------");

/* ---------------------------------- no 3 ---------------------------------- */
function makeRectangle(panjang, lebar) {
    for (var i = 1; i <= lebar; i++) {
        var hash = "";
        for (var j = 1; j <= panjang; j++) {
            hash += "#";
        }
        console.log(hash);
    }
}

// TEST CASE

makeRectangle(8, 4)
console.log("--------------------------------------------------------------------");

/* ---------------------------------- no 4 ---------------------------------- */
function makeLadder(sisi) {
    for (var i = 1; i <= sisi; i++) {
        var hash = "";
        for(var j = 1; j <= i; j++) {
            hash += "#";
        }
        console.log(hash);
    }
}

// TEST CASE
makeLadder(7);