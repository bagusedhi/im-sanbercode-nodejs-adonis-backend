/* ---------------------------------- no 1 ---------------------------------- */
function teriak() {
    return "Halo Sanbers!";
}

console.log(teriak());

/* ---------------------------------- no 2 ---------------------------------- */
function kalikan(num1, num2) {
    return num1 * num2; // your code
}

console.log(kalikan(4, 12)) // 48

/* ---------------------------------- no 3 ---------------------------------- */
function introduce(name, age, address, hobby) {
    // your code
    return "Nama saya "+name+", umur saya "+age+" tahun, alamat saya di "+address+", dan saya punya hobby yaitu "+hobby+"!";
}


// TEST CASES
console.log(introduce("Agus", 30, "Jogja", "Gaming")) // Menampilkan "Nama saya Agus, umur saya 30 tahun, alamat saya di Jogja, dan saya punya hobby yaitu Gaming!" 