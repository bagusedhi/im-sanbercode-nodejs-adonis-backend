/* ---------------------------------- no 1 ---------------------------------- */
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 


function dataHandling (inp) {
    var data = "";
    for (var i = 0; i < inp.length; i++) {
        data += "Nomor ID : "+inp[i][0]+"\n";
        data += "Nama Lengkap : "+inp[i][1]+"\n";
        data += "TTL : "+inp[i][2]+" "+inp[i][3]+"\n";
        data += "Hobi : "+inp[i][4]+"\n";
        data += "\n";
    }
    return data;
}

console.log(dataHandling(input));

/* ---------------------------------- no 2 ---------------------------------- */
function balikKata (word) {
    var balikan = "";
    for (var i = word.length - 1; i >= 0; i--) {
        balikan += word[i];
    }
    return balikan;
}

console.log(balikKata("SanberCode")) 
console.log(balikKata("racecar")) 
console.log(balikKata("kasur rusak"))
console.log(balikKata("haji ijah"))
console.log(balikKata("I am Sanbers"))
