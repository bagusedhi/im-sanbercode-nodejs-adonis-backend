function arrayToObject(arr) {
    var template = ["firstName", "lastName", "Gender", "age"];
    if (arr != "") {
        for (var i = 0; i < arr.length; i++) {
            var obj2 = {};
            for (var j = 0; j < template.length; j++) {
                if (j == 3) {
                    var now = new Date();
                    var thisYear = now.getFullYear();
                    if (Number(arr[i][j]) <= Number(thisYear)) {
                        obj2[template[j]] = thisYear - arr[i][j];
                    } else {
                        obj2[template[j]] = "Invalid Birth Year";
                    }
                } else {
                    obj2[template[j]] = arr[i][j];
                }
            }
            console.log(`${i + 1}. ${arr[i][0] + " " + arr[i][1]} :`,obj2);
            console.log("\n");
        }
    } else {
        console.log("");
    }
    // console.log(arr[1].length);
}

// Driver Code
var people = [
    ["Bruce", "Banner", "male", 1975],
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

/* ---------------------------------- no 2 ---------------------------------- */
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrY = [];
    //your code here
    if (arrPenumpang != "") {
        for(var i = 0; i < arrPenumpang.length; i++) {
            var obj = {
                penumpang : arrPenumpang[i][0],
                naikDari : arrPenumpang[i][1],
                tujuan : arrPenumpang[i][2]                
            }

            var bayar = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000;

            obj.bayar = bayar;
            
            arrY.push(obj);
        }
        return arrY;
    } else {
        return [];
    }
}

//TEST CASE
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]
console.log('\n');
/* ---------------------------------- no 3 ---------------------------------- */
function nilaiTertinggi(siswa) {
    // Code disini
    var output = {};
    for(var i = 0; i < siswa.length; i++) {
        var current = siswa[i];
        if (!output[current.class]) {
            output[current.class] = {
                name : siswa[i].name,
                score : siswa[i].score,
            };
        } else if (current.score > output[current.class].score) {
            output[current.class] = {
                name : siswa[i].name,
                score : siswa[i].score,
            };
        }
    }

    return output;
}
  
  // TEST CASE
  console.log(nilaiTertinggi([
    {
      name: 'Asep',
      score: 90,
      class: 'adonis'
    },  
    {
      name: 'Ahmad',
      score: 85,
      class: 'vuejs'
    },
    {
      name: 'Regi',
      score: 74,
      class: 'adonis'
    },
    {
      name: 'Afrida',
      score: 78,
      class: 'reactjs'
    }
  ]));
  
  // OUTPUT:
  
  // {
  //   adonis: { name: 'Asep', score: 90 },
  //   vuejs: { name: 'Ahmad', score: 85 },
  //   reactjs: { name: 'Afrida', score: 78}
  // }
  
  
  console.log(nilaiTertinggi([
    {
      name: 'Bondra',
      score: 100,
      class: 'adonis'
    },
    {
      name: 'Putri',
      score: 76,
      class: 'laravel'
    },
    {
      name: 'Iqbal',
      score: 92,
      class: 'adonis'
    },
    {
      name: 'Tyar',
      score: 71,
      class: 'laravel'
    },
    {
      name: 'Hilmy',
      score: 80,
      class: 'vuejs'
    }
  ]));
  
  // {
  //   adonis: { name: 'Bondra', score: 100 },
  //   laravel: { name: 'Putri', score: 76 },
  //   vuejs: { name: 'Hilmy', score: 80 }
  // }